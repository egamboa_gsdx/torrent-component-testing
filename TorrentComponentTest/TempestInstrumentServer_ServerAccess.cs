﻿using RemObjects.SDK;
using TempestInstrumentServer;

namespace TorrentComponentTest
{
    interface IServerAccess
    {
        IDatabaseServerService DatabaseServerService { get; }
        IDatabaseServerService_Async DatabaseServerServiceAsync { get; }
        IFileSystemServerService FileSystemServerService { get; }
        IFileSystemServerService_Async FileSystemServerServiceAsync { get; }
        IInstrumentServerService InstrumentServerService { get; }
        IInstrumentServerService_Async InstrumentServerServiceAsync { get; }
        ILisServerService LisServerService { get; }
        ILisServerService_Async LisServerServiceAsync { get; }
        ILogInService LogInService { get; }
        ILogInService_Async LogInServiceAsync { get; }
        IPrintServerService PrintServerService { get; }
        IPrintServerService_Async PrintServerServiceAsync { get; }
    }

    public class ServerAccess : IServerAccess
    {
        private System.String _serverUrl;
        private IClientChannel _clientChannel;
        private IMessage _message;

        public System.String ServerUrl
        {
            get
            {
                return this._serverUrl;
            }
        }

        public ServerAccess()
        {
            this._serverUrl = "http://192.168.1.33:8123";
            this._clientChannel = ClientChannel.ChannelMatchingTargetUri(this._serverUrl);
            this._message = Message.MessageMatchingTargetUri(this._serverUrl);
        }

        public IDatabaseServerService DatabaseServerService
        {
            get
            {
                return CoDatabaseServerService.Create(this._message, this._clientChannel);
            }
        }

        public IDatabaseServerService_Async DatabaseServerServiceAsync
        {
            get
            {
                return CoDatabaseServerServiceAsync.Create(this._message, this._clientChannel);
            }
        }

        public IFileSystemServerService FileSystemServerService
        {
            get
            {
                return CoFileSystemServerService.Create(this._message, this._clientChannel);
            }
        }

        public IFileSystemServerService_Async FileSystemServerServiceAsync
        {
            get
            {
                return CoFileSystemServerServiceAsync.Create(this._message, this._clientChannel);
            }
        }

        public IInstrumentServerService InstrumentServerService
        {
            get
            {
                return CoInstrumentServerService.Create(this._message, this._clientChannel);
            }
        }

        public IInstrumentServerService_Async InstrumentServerServiceAsync
        {
            get
            {
                return CoInstrumentServerServiceAsync.Create(this._message, this._clientChannel);
            }
        }

        public ILisServerService LisServerService
        {
            get
            {
                return CoLisServerService.Create(this._message, this._clientChannel);
            }
        }

        public ILisServerService_Async LisServerServiceAsync
        {
            get
            {
                return CoLisServerServiceAsync.Create(this._message, this._clientChannel);
            }
        }

        public ILogInService LogInService
        {
            get
            {
                return CoLogInService.Create(this._message, this._clientChannel);
            }
        }

        public ILogInService_Async LogInServiceAsync
        {
            get
            {
                return CoLogInServiceAsync.Create(this._message, this._clientChannel);
            }
        }

        public IPrintServerService PrintServerService
        {
            get
            {
                return CoPrintServerService.Create(this._message, this._clientChannel);
            }
        }

        public IPrintServerService_Async PrintServerServiceAsync
        {
            get
            {
                return CoPrintServerServiceAsync.Create(this._message, this._clientChannel);
            }
        }
    }

}
