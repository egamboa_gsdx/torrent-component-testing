﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TempestInstrumentServer;

namespace TorrentComponentTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static InstrumentCommunication _instrumentComm;
        private bool isConnected;
        public MainWindow()
        {
            InitializeComponent();
            isConnected = false;
            var ipAddress = "192.168.1.33"; //subject to change...
            var ch = new RemObjects.SDK.IpSuperHttpClientChannel
            {
                MaxPackageSize = 6656000,
                TargetUrl = $"http://{ipAddress}:{8123}/bin"
            };

            var mes = new RemObjects.SDK.BinMessage { EnforceMaxMessageSize = false };
            var logInService = new LogInService_Proxy(mes, ch);
            if (logInService.Ping(25) == 25)
            {
                logInService.Login("8123");
                _instrumentComm = new InstrumentCommunication(mes, ch);
            }

            _instrumentComm.HardwareInitEventAction = HandleInitEvent;
            _instrumentComm.HardwareInitCompletedAction = HandleCompletedInit;
            StatusTextBox.Text += "Starting insrument initialization...\n";
            _instrumentComm.InstrumentService.StartInitInstrument();

        }

        private void HandleInitEvent(HardwareInitEventArgs args)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                StatusTextBox.Text += $"Init Progress: {args.Progress} ({args.StepPassed})\n";
                StatusTextBox.ScrollToEnd();
            }));
            
        }

        private void HandleCompletedInit(bool passed)
        {
            if (passed)
            {
                _instrumentComm.HomeProgressEventAction = HandleHomeEvent;
                _instrumentComm.HomeCompletedEventAction = HandleCompletedHome;
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    StatusTextBox.Text += "Start homing...\n";
                    StatusTextBox.ScrollToEnd();
                }));
                _instrumentComm.InstrumentService.StartHomeInstrument();
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    StatusTextBox.Text += "Homing started...\n";
                    StatusTextBox.ScrollToEnd();
                }));
            }
        }
        
        private  async void HandleHomeEvent(HomeProgressEventArgs args)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                StatusTextBox.Text += $"Home Progress: {args.Progress}\n";
                StatusTextBox.ScrollToEnd();
            })).Wait();
        }

        private  void HandleCompletedHome(bool passed)
        {
            if (passed)
            {
                isConnected = true;
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    StatusTextBox.Text += "Homing complete!\n";
                    StatusTextBox.ScrollToEnd();
                }));
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void testRunBtn_Click(object sender, RoutedEventArgs e)
        {
            if (isConnected)
            {
                StatusTextBox.Text += "Dispensing tips routine started...\n";
                StatusTextBox.ScrollToEnd();
                var args = new DisposableTipCountUpdateEventArgs()
                {
                    TipPositions = Enumerable.Repeat(true, 96).ToArray(),
                    TipTrayIndex = 0
                };

                _instrumentComm.OnDisposableTipCountUpdate(args);
                _instrumentComm.InstrumentService.CloseTipTray(0);

                for (int i = 0; i < 24; i++)
                {
                    _instrumentComm.InstrumentService.ProvisionDisposableTip();
                    StatusTextBox.Text += $"Provisioning tip {i + 1} into resevoir tray...\n";
                    StatusTextBox.ScrollToEnd();
                }

                for (int i = 0; i < 24; i++)
                {
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        StatusTextBox.Text += $"Picking up tip {i + 1} from resevoir tray...\n";
                        StatusTextBox.ScrollToEnd();
                    }));
                    _instrumentComm.InstrumentService.GetDisposableTipFromReservoir(new TipVerificationArgs());
                    StatusTextBox.Text += $"Disposing tip {i + 1} into discard tray...\n";
                    StatusTextBox.ScrollToEnd();
                }
            }
        }
    }
}
