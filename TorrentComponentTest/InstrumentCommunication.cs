﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemObjects.SDK;
using TempestInstrumentServer;

namespace TorrentComponentTest
{
    public class InstrumentCommunication : IInstrumentEvents
    {
        public InstrumentServerService_Proxy InstrumentService { get; set; }
        public InstrumentServerService_AsyncProxy InstrumentAsyncService { get; set; }

        public Action<AutoAlignEventArgs> AutoAlignEventAction { get; set; }
        public Action<FatalExceptionHappenedEventArgs> FatalExceptionHappenedEventAction { get; set; }
        public Action<HardwareInitEventArgs> HardwareInitEventAction { get; set; }
        public Action<HomeProgressEventArgs> HomeProgressEventAction { get; set; }
        public Action<RinseStartedEventArgs> ProbeRinseStartedEventAction { get; set; }
        public Action InstrumentPowerFailureEventAction { get; set; }
        public Action<DisposableTipCountUpdateEventArgs> InstrumentDisposableTipCountUpdateAction { get; set; }
        public Action<BottleMovedEventArgs> BottleMovedEventAction { get; set; }
        public Action<bool> HardwareInitCompletedAction { get; set; }
        public Action<bool> HomeCompletedEventAction { get; set; }

        /// <summary>
        /// Action is invoked when a software update has failed
        /// </summary>
        public Action SoftwareUpdateFailedAction { get; set; }

        public InstrumentCommunication(IMessage message, IClientChannel channel)
        {
            InstrumentService = new InstrumentServerService_Proxy(message, channel);
            InstrumentAsyncService = new InstrumentServerService_AsyncProxy(message, channel);
            var eventReceiver = new EventReceiver
            {
                Channel = channel,
                Message = message
            };
            eventReceiver.RegisterEventHandler(this);
        }

        /// <summary>
        /// Is fired when an auto alignment step is done
        /// </summary>
        public void OnAutoAlignEvent(AutoAlignEventArgs args)
        {
            AutoAlignEventAction?.Invoke(args);
        }

        /// <summary>
        /// is fired in case an exception happens in a spawned thread of the instrument code
        /// </summary>
        public void OnFatalExceptionHappenedEvent(FatalExceptionHappenedEventArgs args)
        {
            FatalExceptionHappenedEventAction?.Invoke(args);
        }

        /// <summary>
        /// Is fired during the hardware init phase.
        /// </summary>
        public void OnHardwareInitEvent(HardwareInitEventArgs args)
        {
            HardwareInitEventAction?.Invoke(args);
        }

        /// <summary>
        /// Is fired when an update is available about the instrument homing process
        /// </summary>
        public void OnHomeProgressEvent(HomeProgressEventArgs args)
        {
            HomeProgressEventAction?.Invoke(args);
        }

        /// <summary>
        /// is fired when a probe rinse (manual prime) has been done
        /// </summary>
        public void OnRinseStarted(RinseStartedEventArgs args)
        {
            ProbeRinseStartedEventAction?.Invoke(args);
        }

        /// <summary>
        /// is fired when the power on the instrument is lost
        /// </summary>
        public void OnInstrumentPowerFailure()
        {
            InstrumentPowerFailureEventAction?.Invoke();
        }

        /// <summary>
        /// Is fired when the amount of disposable tips has changed
        /// </summary>
        public void OnDisposableTipCountUpdate(DisposableTipCountUpdateEventArgs args)
        {
            InstrumentDisposableTipCountUpdateAction?.Invoke(args);
        }

        /// <summary>
        /// Is fired when a wash, waste or system fluid bottle has been removed or added
        /// </summary>
        public void OnBottleMovedEvent(BottleMovedEventArgs args)
        {
            BottleMovedEventAction?.Invoke(args);
        }

        /// <summary>
        /// Is fired when a software update has failed
        /// </summary>
        public void OnSoftwareUpdateFailed()
        {
            SoftwareUpdateFailedAction?.Invoke();
        }

        public void OnHardwareInitCompletedEvent(bool args)
        {
            HardwareInitCompletedAction?.Invoke(args);
        }

        public void OnHomeCompletedEvent(bool args)
        {
            HomeCompletedEventAction?.Invoke(args);
        }
    }
}
